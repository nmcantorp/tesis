<?php

namespace App\Controller;

use App\Entity\SurveyAnswer;
use App\Entity\SurveyQuestionDetail;
use App\Form\SurveyAnswerType;
use App\Form\SurveyType;
use App\Repository\SurveyAnswerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/survey')]
class SurveyAnswerController extends AbstractController
{
    #[Route('/', name: 'survey_answer_index', methods: ['GET'])]
    public function index(SurveyAnswerRepository $surveyAnswerRepository): Response
    {
        return $this->render('survey_answer/index.html.twig', [
            'survey_answers' => $surveyAnswerRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'survey_answer_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $surveyAnswer = new SurveyAnswer();
        $form = $this->createForm(SurveyType::class, $surveyAnswer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($request->request->all() as $key => $value){
                $surveyQuestionDetail = $entityManager->getRepository(SurveyQuestionDetail::class)->findOneBy(['id'=>$value]);
                if($surveyQuestionDetail!==null) {
                    $user = $this->getUser();
                    $surveyAnswer = new SurveyAnswer();
                    $surveyAnswer->setSurveyQuestionDetail($surveyQuestionDetail);
                    $surveyAnswer->setSurveyQuestion($surveyQuestionDetail->getSurveyQuestion());
                    $surveyAnswer->setUser($user);
                    $entityManager->persist($surveyAnswer);
                }
            }
            $entityManager->flush();

            return $this->redirectToRoute('index_logged', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('survey_answer/new.html.twig', [
            'survey_answer' => $surveyAnswer,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'survey_answer_show', methods: ['GET'])]
    public function show(SurveyAnswer $surveyAnswer): Response
    {
        return $this->render('survey_answer/show.html.twig', [
            'survey_answer' => $surveyAnswer,
        ]);
    }

    #[Route('/{id}/edit', name: 'survey_answer_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SurveyAnswer $surveyAnswer, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(SurveyAnswerType::class, $surveyAnswer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('survey_answer_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('survey_answer/edit.html.twig', [
            'survey_answer' => $surveyAnswer,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'survey_answer_delete', methods: ['POST'])]
    public function delete(Request $request, SurveyAnswer $surveyAnswer, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$surveyAnswer->getId(), $request->request->get('_token'))) {
            $entityManager->remove($surveyAnswer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('survey_answer_index', [], Response::HTTP_SEE_OTHER);
    }
}
