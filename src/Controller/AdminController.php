<?php

namespace App\Controller;

use App\Services\ExportService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
#[Route('/admin', name: 'admin')]
class AdminController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    #[Route('/download_user', name: 'download_user')]
    public function users(ExportService $export): Response
    {
        $responseService=$export->exportUsers();
        return $this->file($responseService['temp_file'], $responseService['file_name'], ResponseHeaderBag::DISPOSITION_INLINE);
    }

    #[Route('/download_user_games', name: 'download_user_games')]
    public function games(ExportService $export): Response
    {
        $responseService=$export->exportUsersWithGames();
        return $this->file($responseService['temp_file'], $responseService['file_name'], ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
