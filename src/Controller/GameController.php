<?php

namespace App\Controller;

use App\Entity\Game;
use App\Repository\UserRepository;
use App\Services\GameService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/', name: 'game')]
class GameController extends AbstractController
{
    #[Route('game/{reactRouting}', name: 'index_game', requirements: ["reactRouting" => ".+"], defaults: ["reactRouting" => null])]
    public function index(Request $request): Response
    {
        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
        ]);
    }

    #[Route('/new_game', name: 'new_game', methods: ["GET"])]
    public function newGame(GameService $gameService): RedirectResponse
    {
        /** @var UserRepository $user */
        $user = $this->getUser();
        $games = $gameService->getGameFromUser($user);

        if (isset($games['Activo']) && count($games['Activo']) > 0) {
            $gameService->cancelGame($games['Activo'][0]['id_game']);
        }

        return $this->redirectToRoute('gameindex_game');
    }

    #[Route('/save_game', name: 'save_game', methods: ["POST"])]
    public function saveGame(Request $request, GameService $gameService)
    {
        $body = json_decode($request->getContent(), true);
        $idGame = $gameService->saveSimulator($body['game'], json_decode($body['simulador'], true));
        return $this->json(['idGame' => $idGame]);
    }

    #[Route('/get_game/{game}', name: 'get_game', defaults: ["game" => null], methods: ["GET"])]
    public function getSimulatorInfo(Game $game, GameService $gameService)
    {
        return $this->json(json_encode($game->getSimulatorInfo()));
    }
}
