<?php

namespace App\Controller\Api;

use App\Entity\Game;
use App\Repository\UserRepository;
use App\Services\GameService;
use App\Services\ProfileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_profile')]
class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'api_profile')]
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/Api/ProfileController.php',
        ]);
    }

    #[Route('/new_profile', name: 'new_profile', methods: 'POST')]
    public function newProfile(Request $request, ProfileService $profileGame){
        $error = 0;
        $message = "";
        $carsInfo = json_decode($request->getContent(), true);
        $user = $this->getUser();
        $result = $profileGame->saveProfile($carsInfo, $user);
        $path ="/";

        return $this->json([
            'message' => $message,
            'path_next' => $path,
            'error' => $error,
        ]);
    }
}
