<?php

namespace App\Controller\Api;

use App\Entity\Game;
use App\Entity\User;
use App\Services\BalloonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
#[Route('/api/balloon', name: 'api_balloon')]
class BalloonController extends AbstractController
{
    #[Route('/', name: 'get_balloon')]
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/Api/BalloonController.php',
        ]);
    }

    #[Route('/save_balloon', name: 'save_balloon', methods: 'POST')]
    public function addBalloonsInfo(Request $request, BalloonService $balloon){
        $error = 0;
        $message = "";
        $balloons = json_decode($request->getContent(), true);
        $user = $this->getUser();
        $result = $balloon->saveBalloon($balloons, $user);
        /** @var Game $result */
        if($result->getCars()->first()){
            $path = '/game/profile';
        }else{
            $path = '/game/cars';
        }

        return $this->json([
            'message' => $message,
            'path_next' => $path,
            'error' => $error,
        ]);

    }
}
