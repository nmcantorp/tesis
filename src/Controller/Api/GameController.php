<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\GameService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/api', name: 'api_principal')]
class GameController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route('/game', name: 'api_game', methods: 'GET')]
    public function index(GameService $gameService): Response
    {
        $error = 0;
        $message = "";
        $games=[];
        try{
            /** @var UserRepository $user */
            $user = $this->getUser();
            $games = $gameService->getGameFromUser($user);
        }catch (\Exception $e){
            $error = 1;
            $message=$e->getMessage();
        }
        return $this->json([
            'message' => $message,
            'games' => $games,
            'error' => $error,
        ]);
    }

    #[Route('/new_game', name: 'new_game', methods: 'POST')]
    public function newGame(GameService $gameService){
        $error = 0;
        $message = "";
        try{
            /** @var UserRepository $user */
            $user = $this->getUser();
            $game = $gameService->setNewGame($user);
        }catch (\Exception $e){
            $error = 1;
            $message=$e->getMessage();
        }
        return $this->json([
            'message' => $message,
            'games' => $game,
            'error' => $error,
        ]);
    }
}
