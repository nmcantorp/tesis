<?php

namespace App\Repository;

use App\Entity\Balloon;
use App\Entity\Cars;
use App\Entity\Game;
use App\Entity\ProfileGame;
use App\Entity\SurveyAnswer;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function getAllUserWithGamesActive()
    {
        return $this->createQueryBuilder('u')
            ->join(Game::class, 'g', 'WITH', 'g.User = u.id')
            ->leftJoin(Balloon::class, 'b', 'WITH', 'b.Game = g.id')
            ->leftJoin(Cars::class, 'c', 'WITH', 'c.Game = g.id')
            ->select([
                'u.id',
                'u.email',
                'COALESCE(b.clickTotal, \'Sin Jugar\') as balloonTotal',
                'COALESCE(b.clickAvailable, \'Sin Jugar\') as balloonAvailable',
                'COALESCE(c.clickTotal, \'Sin Jugar\') as totalCars',
                'COALESCE(c.clickFast, \'Sin Jugar\') as clickFast',
                'COALESCE(c.clickAvailable, \'Sin Jugar\') as availableCars',
                'g.simulatorInfo'
            ])
            ->where('g.status = 1')
            ->orderBy('g.status', 'asc')
            ->addOrderBy('g.date_created', 'desc')
            ->getQuery()
            ->getResult();
    }

    public function getUserWithGames(User $user)
    {
        return $this->createQueryBuilder('u')
            ->join(Game::class, 'g', 'WITH', 'g.User = u.id')
            ->leftJoin(Balloon::class, 'b', 'WITH', 'b.Game = g.id')
            ->leftJoin(Cars::class, 'c', 'WITH', 'c.Game = g.id')
            ->leftJoin(ProfileGame::class, 'p', 'WITH', 'p.game = g.id')
            ->select([
                'u.id',
                'u.email',
                'g.id as id_game',
                'g.status',
                'b.clickTotal as balloonTotal',
                'b.clickAvailable as balloonAvailable',
                'c.clickTotal as totalCars',
                'c.clickFast as clickFast',
                'c.clickAvailable as availableCars',
                'p.letterProfile as letterProfile'
            ])
            ->where('u = :user')
            ->setParameter('user', $user)
            ->orderBy('g.status', 'asc')
            ->addOrderBy('g.date_created', 'desc')
            ->getQuery()
            ->getResult();
    }

    public function getUsersWithExams()
    {
        return $this->createQueryBuilder('u')
            ->leftJoin(SurveyAnswer::class, 'a', 'WITH', 'u.id = a.User')
            ->select(['u', 'u.email', 'u.roles', 'a.createdAt'])
            ->groupBy('u.id')
            ->getQuery()
            ->getResult();
    }

}
