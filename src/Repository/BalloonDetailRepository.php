<?php

namespace App\Repository;

use App\Entity\BalloonDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BalloonDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method BalloonDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method BalloonDetail[]    findAll()
 * @method BalloonDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BalloonDetailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BalloonDetail::class);
    }

    // /**
    //  * @return BalloonDetail[] Returns an array of BalloonDetail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BalloonDetail
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
