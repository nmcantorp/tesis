<?php

namespace App\Entity;

use App\Repository\SurveyQuestionDetailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SurveyQuestionDetailRepository::class)]
class SurveyQuestionDetail
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: SurveyQuestion::class, inversedBy: 'labelOption')]
    #[ORM\JoinColumn(nullable: false)]
    private $SurveyQuestion;

    #[ORM\Column(type: 'string', length: 255)]
    private $labelOption;

    #[ORM\Column(type: 'datetime_immutable')]
    private $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    private $updatedAt;

    #[ORM\OneToMany(mappedBy: 'SurveyQuestionDetail', targetEntity: SurveyAnswer::class)]
    private $surveyAnswers;


    public function __construct()
    {
        $this->surveyAnswers = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->labelOption;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSurveyQuestion(): ?SurveyQuestion
    {
        return $this->SurveyQuestion;
    }

    public function setSurveyQuestion(?SurveyQuestion $SurveyQuestion): self
    {
        $this->SurveyQuestion = $SurveyQuestion;

        return $this;
    }

    public function getOption(): ?string
    {
        return $this->labelOption;
    }

    public function setOption(string $labelOption): self
    {
        $this->labelOption = $labelOption;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|SurveyAnswer[]
     */
    public function getSurveyAnswers(): Collection
    {
        return $this->surveyAnswers;
    }

    public function addSurveyAnswer(SurveyAnswer $surveyAnswer): self
    {
        if (!$this->surveyAnswers->contains($surveyAnswer)) {
            $this->surveyAnswers[] = $surveyAnswer;
            $surveyAnswer->setSurveyQuestionDetail($this);
        }

        return $this;
    }

    public function removeSurveyAnswer(SurveyAnswer $surveyAnswer): self
    {
        if ($this->surveyAnswers->removeElement($surveyAnswer)) {
            // set the owning side to null (unless already changed)
            if ($surveyAnswer->getSurveyQuestionDetail() === $this) {
                $surveyAnswer->setSurveyQuestionDetail(null);
            }
        }

        return $this;
    }


}
