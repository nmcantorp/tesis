<?php

namespace App\Entity;

use App\Repository\ProfileGameRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProfileGameRepository::class)]
class ProfileGame
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'float')]
    private $balloonAverage;

    #[ORM\Column(type: 'float')]
    private $carsAverage;

    #[ORM\Column(type: 'float')]
    private $profileAverage;

    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'profileGames')]
    #[ORM\JoinColumn(nullable: false)]
    private $game;

    #[ORM\Column(type: 'string', length: 10)]
    private $letterProfile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBalloonAverage(): ?int
    {
        return $this->balloonAverage;
    }

    public function setBalloonAverage(float $balloonAverage): self
    {
        $this->balloonAverage = $balloonAverage;

        return $this;
    }

    public function getCarsAverage(): ?int
    {
        return $this->carsAverage;
    }

    public function setCarsAverage(float $carsAverage): self
    {
        $this->carsAverage = $carsAverage;

        return $this;
    }

    public function getProfileAverage(): ?float
    {
        return $this->profileAverage;
    }

    public function setProfileAverage(float $profileAverage): self
    {
        $this->profileAverage = $profileAverage;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getLetterProfile(): ?string
    {
        return $this->letterProfile;
    }

    public function setLetterProfile(string $letterProfile): self
    {
        $this->letterProfile = $letterProfile;

        return $this;
    }
}
