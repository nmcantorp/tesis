<?php

namespace App\Entity;

use App\Repository\SurveyAnswerRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SurveyAnswerRepository::class)]
class SurveyAnswer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: SurveyQuestion::class, inversedBy: 'surveyAnswers')]
    #[ORM\JoinColumn(nullable: false)]
    private $SurveyQuestion;

    #[ORM\ManyToOne(targetEntity: SurveyQuestionDetail::class, inversedBy: 'surveyAnswers')]
    #[ORM\JoinColumn(nullable: false)]
    private $SurveyQuestionDetail;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'surveyAnswers')]
    #[ORM\JoinColumn(nullable: false)]
    private $User;

    #[ORM\Column(type: 'datetime_immutable')]
    private $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSurveyQuestion(): ?SurveyQuestion
    {
        return $this->SurveyQuestion;
    }

    public function setSurveyQuestion(?SurveyQuestion $SurveyQuestion): self
    {
        $this->SurveyQuestion = $SurveyQuestion;

        return $this;
    }

    public function getSurveyQuestionDetail(): ?SurveyQuestionDetail
    {
        return $this->SurveyQuestionDetail;
    }

    public function setSurveyQuestionDetail(?SurveyQuestionDetail $SurveyQuestionDetail): self
    {
        $this->SurveyQuestionDetail = $SurveyQuestionDetail;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
