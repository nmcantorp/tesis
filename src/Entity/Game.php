<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameRepository::class)]
class Game
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'smallint')]
    private $status;

    #[ORM\Column(type: 'datetime')]
    private $date_created;

    #[ORM\Column(type: 'datetime_immutable')]
    private $date_updated_at;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'games')]
    #[ORM\JoinColumn(nullable: false)]
    private $User;

    #[ORM\OneToMany(mappedBy: 'Game', targetEntity: Balloon::class)]
    private $balloons;

    #[ORM\OneToMany(mappedBy: 'Game', targetEntity: Cars::class)]
    private $cars;

    #[ORM\OneToMany(mappedBy: 'game', targetEntity: ProfileGame::class)]
    private $profileGames;

    #[ORM\Column(type: 'json', nullable: true)]
    private $simulatorInfo = [];

    public function __construct()
    {
        $this->balloons = new ArrayCollection();
        $this->cars = new ArrayCollection();
        $this->profileGames = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->date_created;
    }

    public function setDateCreated(\DateTimeInterface $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->date_updated_at;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $date_updated_at): self
    {
        $this->date_updated_at = $date_updated_at;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection|Balloon[]
     */
    public function getBalloons(): Collection
    {
        return $this->balloons;
    }

    public function addBalloon(Balloon $balloon): self
    {
        if (!$this->balloons->contains($balloon)) {
            $this->balloons[] = $balloon;
            $balloon->setGame($this);
        }

        return $this;
    }

    public function removeBalloon(Balloon $balloon): self
    {
        if ($this->balloons->removeElement($balloon)) {
            // set the owning side to null (unless already changed)
            if ($balloon->getGame() === $this) {
                $balloon->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cars[]
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    public function addCar(Cars $car): self
    {
        if (!$this->cars->contains($car)) {
            $this->cars[] = $car;
            $car->setGame($this);
        }

        return $this;
    }

    public function removeCar(Cars $car): self
    {
        if ($this->cars->removeElement($car)) {
            // set the owning side to null (unless already changed)
            if ($car->getGame() === $this) {
                $car->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProfileGame[]
     */
    public function getProfileGames(): Collection
    {
        return $this->profileGames;
    }

    public function addProfileGame(ProfileGame $profileGame): self
    {
        if (!$this->profileGames->contains($profileGame)) {
            $this->profileGames[] = $profileGame;
            $profileGame->setGame($this);
        }

        return $this;
    }

    public function removeProfileGame(ProfileGame $profileGame): self
    {
        if ($this->profileGames->removeElement($profileGame)) {
            // set the owning side to null (unless already changed)
            if ($profileGame->getGame() === $this) {
                $profileGame->setGame(null);
            }
        }

        return $this;
    }

    public function getSimulatorInfo(): ?array
    {
        return $this->simulatorInfo;
    }

    public function setSimulatorInfo(?array $simulatorInfo): self
    {
        $this->simulatorInfo = $simulatorInfo;

        return $this;
    }
}
