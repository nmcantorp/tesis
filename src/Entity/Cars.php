<?php

namespace App\Entity;

use App\Repository\CarsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CarsRepository::class)]
class Cars
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $clickTotal;

    #[ORM\Column(type: 'integer')]
    private $clickAvailable;

    #[ORM\Column(type: 'datetime_immutable')]
    private $dateCreatedAt;

    #[ORM\Column(type: 'datetime_immutable')]
    private $dateUpdateAt;

    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'cars')]
    #[ORM\JoinColumn(nullable: false)]
    private $Game;

    #[ORM\Column(type: 'integer', options: ['default'=>0])]
    private $clickFast;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClickTotal(): ?int
    {
        return $this->clickTotal;
    }

    public function setClickTotal(int $clickTotal): self
    {
        $this->clickTotal = $clickTotal;

        return $this;
    }

    public function getClickAvailable(): ?int
    {
        return $this->clickAvailable;
    }

    public function setClickAvailable(int $clickAvailable): self
    {
        $this->clickAvailable = $clickAvailable;

        return $this;
    }

    public function getDateCreatedAt(): ?\DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(\DateTimeImmutable $dateCreatedAt): self
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getDateUpdateAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdateAt;
    }

    public function setDateUpdateAt(\DateTimeImmutable $dateUpdateAt): self
    {
        $this->dateUpdateAt = $dateUpdateAt;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->Game;
    }

    public function setGame(?Game $Game): self
    {
        $this->Game = $Game;

        return $this;
    }

    public function getClickFast(): ?int
    {
        return $this->clickFast;
    }

    public function setClickFast(int $clickFast): self
    {
        $this->clickFast = $clickFast;

        return $this;
    }
}
