<?php

namespace App\Entity;

use App\Repository\BalloonDetailRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BalloonDetailRepository::class)]
class BalloonDetail
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $numberBalloon;

    #[ORM\Column(type: 'integer')]
    private $clickTotal;

    #[ORM\Column(type: 'integer')]
    private $clickAvailable;

    #[ORM\ManyToOne(targetEntity: Balloon::class, inversedBy: 'balloonDetails')]
    #[ORM\JoinColumn(nullable: false)]
    private $balloon;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberBalloon(): ?int
    {
        return $this->numberBalloon;
    }

    public function setNumberBalloon(int $numberBalloon): self
    {
        $this->numberBalloon = $numberBalloon;

        return $this;
    }

    public function getClickTotal(): ?int
    {
        return $this->clickTotal;
    }

    public function setClickTotal(int $clickTotal): self
    {
        $this->clickTotal = $clickTotal;

        return $this;
    }

    public function getClickAvailable(): ?int
    {
        return $this->clickAvailable;
    }

    public function setClickAvailable(int $clickAvailable): self
    {
        $this->clickAvailable = $clickAvailable;

        return $this;
    }

    public function getBalloon(): ?Balloon
    {
        return $this->balloon;
    }

    public function setBalloon(?Balloon $balloon): self
    {
        $this->balloon = $balloon;

        return $this;
    }
}
