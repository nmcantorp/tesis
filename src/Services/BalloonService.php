<?php

namespace App\Services;

use App\Entity\Balloon;
use App\Entity\BalloonDetail;
use App\Entity\Game;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class BalloonService
{
    private GameService $game;

    public function __construct(EntityManagerInterface $em, GameService $game)
    {
        $this->em = $em;
        $this->game = $game;
    }

    /**
     * @param $data
     * @param User $user
     * @return Balloon|array
     */
    public function saveBalloon($data, User $user){
        $game = $this->game->getGameFromUser($user)['Activo'][0]??[];
        if(count($data)>0 && count($game)>0){
            $resultSuma = $this->sumDetails($data);
            $game = $this->em->getRepository(Game::class)->findOneBy(['id'=>$game['id_game']]);
            $balloon = new Balloon();
            $balloon->setGame($game);
            $balloon->setClickTotal($resultSuma["totalClick"]);
            $balloon->setClickAvailable(($resultSuma["availableClick"])*10);
            $this->em->persist($balloon);
            foreach ($data as $key => $value){
                $balloonDetails = new BalloonDetail();
                $balloonDetails->setBalloon($balloon);
                $balloonDetails->setClickTotal($value['clicks']);
                $balloonDetails->setClickAvailable(10);
                $balloonDetails->setNumberBalloon($value['numberBalloon']);
                $this->em->persist($balloonDetails);
            }
            $this->em->flush($balloon);
            $this->em->flush($balloonDetails);
            $game = $this->em->getRepository(Game::class)->findOneBy(['id'=>$game->getId()]);
        }
        return $game;
    }

    private function sumDetails($data): array
    {
        $arrayFinal = [
            "totalClick" => 0,
            "availableClick" => 0
        ];
        foreach ($data as $key => $value){
            $arrayFinal["totalClick"] += $value['clicks'];
            $arrayFinal["availableClick"] += 1;
        }

        return $arrayFinal;
    }
}