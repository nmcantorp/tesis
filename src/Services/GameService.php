<?php

namespace App\Services;

use App\Entity\Game;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class GameService
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getGameFromUser(User $user)
    {
        $arrayResult = [];
        $result = $this->em->getRepository(User::class)->getUserWithGames($user);
        if (count($result) > 0) {
            foreach ($result as $key => $item) {
                $arrayResult[($item['status'] == '1') ? 'Activo' : 'Finalizado'][] = $item;
            }
        }
        return $arrayResult;
    }

    public function setNewGame(User $user)
    {
        $game = new Game();
        $game->setUser($user);
        $game->setStatus(1);
        $game->setDateCreated(new \DateTimeImmutable());
        $game->setDateUpdatedAt(new \DateTimeImmutable());

        $this->em->persist($game);
        $this->em->flush($game);

        return $game->getId();
    }

    public function cancelGame($idGame)
    {
        $game = $this->em->getRepository(Game::class)->findOneBy(array('id' => $idGame));
        $game->setStatus(2);
        $this->em->persist($game);
        $this->em->flush();

        return $game->getId();
    }

    public function saveSimulator($idGame, $information)
    {
        $game = $this->em->getRepository(Game::class)->findOneBy(array('id' => $idGame));
        $game->setSimulatorInfo($information);
        $this->em->persist($game);
        $this->em->flush();

        return $game->getId();
    }
}