<?php

namespace App\Services;

use App\Entity\Balloon;
use App\Entity\BalloonDetail;
use App\Entity\Cars;
use App\Entity\Game;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class CarService
{
    private GameService $game;

    public function __construct(EntityManagerInterface $em, GameService $game)
    {
        $this->em = $em;
        $this->game = $game;
    }

    /**
     * @param $data
     * @param User $user
     * @return Balloon|array
     */
    public function saveCars($data, User $user){
        $game = $this->game->getGameFromUser($user)['Activo'][0]??[];
        if(count($data)>0 && count($game)>0){
            $game = $this->em->getRepository(Game::class)->findOneBy(['id'=>$game['id_game']]);
            $cars = new Cars();
            $cars->setGame($game);
            $cars->setClickTotal($data["clicks"]);
            $cars->setClickAvailable($data["available"]);
            $cars->setClickFast($data["clickFast"]);
            $cars->setDateCreatedAt(new \DateTimeImmutable());
            $cars->setDateUpdateAt(new \DateTimeImmutable());
            $this->em->persist($cars);
            $this->em->flush($cars);
            $game = $this->em->getRepository(Game::class)->findOneBy(['id'=>$game->getId()]);
        }
        return $game;
    }
}