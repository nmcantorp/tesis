<?php

namespace App\Services;

use App\Entity\SurveyAnswer;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportService
{
    private EntityManagerInterface $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function exportUsersWithGames(): array
    {
        $userList = $this->em->getRepository(User::class)->getAllUserWithGamesActive();
        $list = [];
        array_walk($userList, function ($user, $index) use (&$list) {
            $list[$index][] = $user['id'];
            $list[$index][] = $user['email'];
            $list[$index][] = $user['balloonTotal'];
            $list[$index][] = $user['balloonAvailable'];
            $list[$index][] = $user['totalCars'];
            $list[$index][] = $user['clickFast'];
            $list[$index][] = $user['availableCars'];
            $list[$index][] = count(is_null($user['simulatorInfo']) ? [] : $user['simulatorInfo']) > 0 ? round((($user['simulatorInfo']["currentTime"] * 2) / 60), 2) . " Minutos" : "0 Minutos";
            $list[$index][] = count(is_null($user['simulatorInfo']) ? [] : $user['simulatorInfo']) > 0 ? ($user['simulatorInfo']["userGame"]["profileName"]) : "-";
            $list[$index][] = count(is_null($user['simulatorInfo']) ? [] : $user['simulatorInfo']) > 0 ? "$" . round($user['simulatorInfo']["availableBalance"], 2) : "-";
            $list[$index][] = count(is_null($user['simulatorInfo']) ? [] : $user['simulatorInfo']) > 0 ? $user['simulatorInfo']["pointsAvailable"] : "-";
            return $user;
        });

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Usuarios Registrados');

        $sheet->getCell('A1')->setValue('Id Usuario');
        $sheet->getCell('B1')->setValue('Email');
        $sheet->getCell('C1')->setValue('Click Total Globos');
        $sheet->getCell('D1')->setValue('Click Disponibles Globos');
        $sheet->getCell('E1')->setValue('Click Total Carros');
        $sheet->getCell('F1')->setValue('Click Rapidos Carros');
        $sheet->getCell('G1')->setValue('Click Disponibles Carros');
        $sheet->getCell('H1')->setValue('Horas en el Simulador');
        $sheet->getCell('I1')->setValue('Perfil');
        $sheet->getCell('J1')->setValue('Saldo Disponible');
        $sheet->getCell('K1')->setValue('Puntos Obtenidos');

        $sheet->fromArray($list, null, 'A2', true);

        $nameFile = 'clicks_users_games_' . uniqid() . '.xlsx';
        $write = new Xlsx($spreadsheet);
        $temp_file = tempnam(sys_get_temp_dir(), $nameFile);

        // Guardar el archivo de excel en el directorio temporal del sistema
        $write->save($temp_file);

        return ['temp_file' => $temp_file, 'file_name' => $nameFile];

    }

    #[ArrayShape(['temp_file' => "false|string", 'file_name' => "string"])]
    public function exportUsers(): array
    {
        $userList = $this->em->getRepository(User::class)->getUsersWithExams();
        $userList = array_map(function ($user) {
            if (count($user['roles']) <= 0) {
                $user['roles'][] = 'ROLE_USER';
            }
            $roles = implode(',', $user['roles']);
            $user['roles'] = $roles;
            $user['createdAt'] = (is_null($user['createdAt'])) ? "" : $user['createdAt']->format('Y-m-d H:m:s');
            $answers = $user[0]->getSurveyAnswers()->toArray();
            $answersWithQuestions = array_map(function ($answer) {
                return $answer->getSurveyQuestion() . " - RTA: " . $answer->getSurveyQuestionDetail()->getOption();
            }, $answers);
            $user = array_merge($user, $answersWithQuestions);
//            rsort($user);
            unset($user[0]);
            return $user;
        }, $userList);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Usuarios Registrados');

        $sheet->getCell('A1')->setValue('Correo');
        $sheet->getCell('B1')->setValue('Roles');
        $sheet->getCell('C1')->setValue('Fecha Examen');
        $sheet->getCell('D1')->setValue('Respuestas Examenes');

        $sheet->fromArray($userList, null, 'A2', true);

        $nameFile = 'users_registered_' . uniqid() . '.xlsx';
        $write = new Xlsx($spreadsheet);
        $temp_file = tempnam(sys_get_temp_dir(), $nameFile);

        // Guardar el archivo de excel en el directorio temporal del sistema
        $write->save($temp_file);

        return ['temp_file' => $temp_file, 'file_name' => $nameFile];
    }
}