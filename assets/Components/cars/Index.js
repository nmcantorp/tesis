import React, {Component} from 'react';
import {Chart} from "react-google-charts";
import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Fab,
    Modal, Typography
} from "@mui/material";
import NavigationIcon from '@mui/icons-material/Navigation';
import {Link} from "react-router-dom";
import axios from "axios";
import LoadingUtils from "../utils/LoadingUtils";
import VideoTutorial from "../utils/VideoTutorial";

class Index extends Component {
    constructor() {
        super();
        this.state = {
            started: false,
            gas: 100,
            speed: 0,
            open: false,
            cash: 6000,
            time: 0,
            selectedOption: "",
            totalDistance: 1000,
            stages: 5,
            available: 5,
            clickFast: 0,
            clickTotal: 0,
            clicks: 0,
            openDialog: false,
            status: '',
            finish: false
        }
        this.state.distanceTraveled = this.state.totalDistance / this.state.stages;
        this.state.initialCash = this.state.cash;

        this.setOpen = this.setOpen.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.iterations = this.iterations.bind(this);
        this.handlerDialog = this.handlerDialog.bind(this);
    }

    componentDidMount() {
        this.refs.tutorial.handleDialog(true);
    }

    saveInfo() {
        this.loading(true);
        axios.post(this.props.images_info.configInfo.urlNewCars, JSON.stringify(this.state))
            .then(result => {
                if (result.status == 200 && result.data.error == 0) {
                    let response = result.data;
                    window.location = response.path_next;
                }
            })
            .catch(error => {
                this.loading(false);
            });
    }

    setOpen(value) {
        this.setState({open: value});
    }

    resetInfo() {
        this.setState({
            gas: 100,
            speed: 0,
            cash: 6000,
            time: 0
        });
    }

    onValueChange(event) {
        this.setState({
            selectedOption: event.target.value
        });
    }

    handlerDialog(value) {
        this.setState({
            openDialog: value
        });

        if (value) {
            this.setState({
                finish: true,
                open: false
            });
            this.saveInfo();
        }
    }

    iterations() {

        this.setState({
            stages: (this.state.stages - 1)
        });
        let distance = (this.state.totalDistance / 10) - (20 * this.state.stages - 1);
        let car = document.getElementById('car_image');
        car.setAttribute('style', `left: ${distance}% !important`);

        if (this.state.stages >= 1) {
            let clicks = this.state.clicks;
            if (this.state.gas <= 0) {
                // this.updateGas(false);
                this.updateSpeed(false);
                this.determineSpeed();
                this.handlerDialog(true);
            } else {
                console.log('con gasolina')
                this.updateSpeed(true);
                this.determineSpeed();
            }
            clicks++;
            this.setState({
                clicks: clicks
            });
        } else {
            this.setState({
                status: 'win'
            });
            this.handlerDialog(true);
        }

    }

    updateGas(band = true) {
        let gas = 0;
        if (band) {
            gas = this.state.gas;
        }
        this.setState({
            gas
        });
    }

    updateSpeed(band = true) {
        let speed = 0;
        let clickFast = this.state.clickFast;
        if (band) {
            if (this.state.selectedOption === 'Despacio') {
                speed = 30;
            } else {
                clickFast++;
                speed = 80;
            }
        }
        this.setState({
            speed,
            clickFast
        });
    }

    determineSpeed() {
        const FAST = 4;
        const SLOW = 10;
        if (this.state.selectedOption === 'Despacio') {
            this.setState({
                gas: ((this.state.gas - this.consumption(10, 20)) < 0) ? 0 : (this.state.gas - this.consumption(10, 20)),
                time: this.state.time + SLOW,
                cash: this.state.cash - (SLOW * 100)
            });
        } else {
            this.setState({
                gas: ((this.state.gas - this.consumption(18, 40)) < 0) ? 0 : (this.state.gas - this.consumption(18, 40)),
                time: this.state.time + FAST,
                cash: this.state.cash - (FAST * 100),
                clickFast: this.state.clickFast + 1
            });
        }
    }

    consumption(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    loading(value) {
        this.setState({
            loading: value
        });
    }

    render() {
        const style = {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -16rem)',
            width: '50%',
            backgroundColor: '#f36060c7',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
        };
        return (
            <div>
                <VideoTutorial tutorial={this.props.images_info.itemVideos.carrito} ref={"tutorial"}/>
                <LoadingUtils loadingStatus={this.state.loading}/>
                <div className="container-fluid gtco-features balloons_content" id="about">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <h4> Vamos a movilizarnos: </h4>
                                <div className="row">
                                    <div className="col-md-12">
                                        <img src={this.props.images_info.itemImages.way} alt="way" id={"route_city"}/>
                                        <div className="way_class" id={"car_image"}>
                                            <img src={this.props.images_info.itemImages.car} alt="way" id={"car_city"}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="row" id={"tacometros"}>
                                    <div className="col-md-6">
                                        <Chart
                                            chartType="Gauge"
                                            data={[["Label", "Value"], ['Gasolina', this.state.gas]]}
                                            width="70%"
                                            height="200px"
                                            options={{
                                                redFrom: 0,
                                                redTo: 10,
                                                yellowFrom: 10,
                                                yellowTo: 25,
                                                minorTicks: 5,
                                            }}
                                            legendToggle
                                        />
                                    </div>
                                    <div className="col-md-6">
                                        <Chart
                                            chartType="Gauge"
                                            data={[["Label", "Value"], ['Velocidad', this.state.speed]]}
                                            width="70%"
                                            height="200px"
                                            options={{
                                                redFrom: 90,
                                                redTo: 100,
                                                yellowFrom: 75,
                                                yellowTo: 90,
                                                minorTicks: 5,
                                            }}
                                            legendToggle
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Link to="/game" className={"menu_principal"}>Menú <i className="fa fa-angle-left"
                                                                          aria-hidden="true"></i></Link>
                </div>
                <Box className={"starter-button"} sx={{'& > :not(style)': {m: 1}}}>
                    <Fab variant="extended" size="small" color="primary" aria-label="add"
                         onClick={() => this.setOpen(true)}>
                        <NavigationIcon sx={{mr: 1}}/>
                        Encender
                    </Fab>
                </Box>

                <Modal
                    hideBackdrop
                    open={this.state.open}
                    onClose={() => this.setOpen(false)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box style={style} className={"box-modal-balloons"}>
                        <div className="card">
                            <div className="card-header">
                                Avance del Carro!
                            </div>
                            <div className="card-body">
                                <h5 className="card-title">Información de recorrido</h5>
                                <div className="card-text text-center">
                                    <div className="row">
                                        <div className="col-sm-6 text-dark">Tiempo Actual</div>
                                        <div className="col-sm-6">{this.state.time}</div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-6 text-dark">Dinero Actual</div>
                                        <div className="col-sm-6">${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.cash)}</div>
                                    </div>
                                    <div className="row justify-content-center">
                                        <h5>A qué velocidad deseas realizar tu viaje?</h5>
                                        <div className="col-md-12">
                                            <table className="table table-responsive-lg">
                                                <caption
                                                    className={"font-weight-bold text-dark text-center h2"}>Recuerda que
                                                    entre más rápido vas más dinero ganarás y más gasolina gastarás.
                                                </caption>
                                                <tbody>
                                                <tr>
                                                    <td className="text-center">
                                                        <div className="radio form-check">
                                                            <label className={"form-check-label"}>
                                                                <input
                                                                    type="radio"
                                                                    name="speedRadio"
                                                                    value="Despacio"
                                                                    checked={this.state.selectedOption === "Despacio"}
                                                                    onChange={this.onValueChange}
                                                                    className={"form-check-input"}
                                                                />
                                                                Despacio
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td className="text-center">
                                                        <div className="radio form-check">
                                                            <label className={"form-check-label"}>
                                                                <input
                                                                    type="radio"
                                                                    name="speedRadio"
                                                                    value="Rapido"
                                                                    checked={this.state.selectedOption === "Rapido"}
                                                                    onChange={this.onValueChange}
                                                                    className={"form-check-input"}
                                                                    disabled={(this.state.finish) ? (true) : (false)}
                                                                />
                                                                Rápido
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-2 col-xs-6">
                                        {(this.state.selectedOption !== "") ? (!this.state.finish) ?
                                                (<button type="button" className="btn btn-success"
                                                         onClick={() => this.iterations()}>
                                                    {(this.state.stages >= 1) ? ("Avanzar!") : ("Finalizar")}
                                                </button>) :
                                                (<button type="button" className="btn btn-success"
                                                         onClick={() => this.setOpen(false)}>Cerrar</button>)
                                            : (<React.Fragment/>)}
                                        {/*{ (!this.state.finish)?
                                            (<button type="button" className="btn btn-success" onClick={()=>this.iterations()}>{(this.state.stages >= 1)?("Avanzar!"):("Finalizar")}</button>):
                                            (<button type="button" className="btn btn-success" onClick={()=>this.setOpen(false)}>Cerrar</button>)
                                        }*/}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Box>
                </Modal>
                <Dialog
                    open={this.state.openDialog}
                    // onClose={this.handleClose(false)}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    {this.state.status === 'win' ? (<div>
                        <DialogTitle id="alert-dialog-title">
                            {"Lo has logrado"}
                        </DialogTitle>
                        <DialogContent>
                            <Typography>
                                ${new Intl.NumberFormat("ES-ES", {
                                style: "currency",
                                currency: "COP"
                            }).format(this.state.cash)} Dinero Ganado
                            </Typography>
                            <hr/>
                            <Typography>
                                {this.state.time} Min
                            </Typography>
                            {/*<DialogContentText id="alert-dialog-description">

                            </DialogContentText>*/}
                        </DialogContent>
                    </div>) : (<div>
                            <DialogTitle id="alert-dialog-title">
                                {"Has perdido, no te queda suficiente gasolina para completar el recorrido"}
                            </DialogTitle>
                            <DialogContent>
                                <Typography>
                                    ${new Intl.NumberFormat("ES-ES", {
                                    style: "currency",
                                    currency: "COP"
                                }).format(this.state.cash)} Dinero Actual
                                </Typography>
                                <hr/>
                                <Typography>
                                    {this.state.time} Min
                                </Typography>
                                {/*<DialogContentText id="alert-dialog-description">

                                </DialogContentText>*/}
                            </DialogContent>
                        </div>
                    )}
                    <DialogActions>
                        <Button onClick={() => this.handlerDialog(false)}>Cerrar</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default Index;