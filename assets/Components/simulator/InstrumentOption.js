import React, {Component} from "react";
import {Box, Button, Card, CardActions, CardContent, CardMedia, Typography} from "@mui/material";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import * as PropTypes from "prop-types";

class InstrumentOption extends Component {
    render() {
        return <Box
            sx={{
                width: "30%",
                height: 200,
                display: "inline-block",
                marginLeft: "2%",
                "&:hover": {
                    backgroundColor: "primary.main",
                    opacity: [0.9, 0.8, 0.7],
                },
            }}
        >
            <Card sx={{minWidth: 275}}>
                <CardContent>
                    <Typography sx={{fontSize: 18}} color="text.black" gutterBottom>
                        {this.props.texto}
                    </Typography>
                    <CardMedia
                        component="img"
                        height="194"
                        image={this.props.imagesInfo}
                        alt="Paella dish"
                    />
                </CardContent>
                <CardActions>
                    <Button size="small"
                            onClick={this.props.onClick}>Agregar <AddShoppingCartIcon/></Button>
                </CardActions>
            </Card>
        </Box>;
    }
}


InstrumentOption.propTypes = {
    imagesInfo: PropTypes.any,
    onClick: PropTypes.func
};

export default InstrumentOption;
