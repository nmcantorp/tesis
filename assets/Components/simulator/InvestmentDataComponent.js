import React, {Component} from "react";
import {Box, Divider, IconButton, Popper, Typography} from "@mui/material";
import {styled} from '@mui/material/styles';
import {
    ArrowDownwardTwoTone,
    ArrowUpwardTwoTone, CheckCircleTwoTone, DoNotDisturbTwoTone,
    DragHandleTwoTone
} from "@mui/icons-material";
import Tooltip, {tooltipClasses} from '@mui/material/Tooltip';
import * as PropTypes from "prop-types";

class InvestmentDataComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: null,
            parentValidation: this.props.selectedForecast,
            openPopper: false
        }

        this.selectForecasts = this.selectForecasts.bind(this);
        this.clearSelectedInfo = this.clearSelectedInfo.bind(this);
    }

    selectForecasts(value) {
        this.setState({
            selected: value
        })
    }

    clearSelectedInfo() {
        this.setState({
            selected: null
        });
    }

    render() {
        const HtmlTooltip = styled(({className, ...props}) => (
            <Tooltip {...props} classes={{popper: className}}/>
        ))(({theme}) => ({
            [`& .${tooltipClasses.tooltip}`]: {
                backgroundColor: '#f5f5f9',
                color: 'rgba(0, 0, 0, 0.87)',
                maxWidth: 220,
                fontSize: theme.typography.pxToRem(12),
                border: '1px solid #dadde9',
            },
        }));
        return <div>
            <div className="row inversion">
                <div className={"col-md-2 nombre-inversion investment"}
                     disabled={!this.props.investment.status}>
                    <strong> {this.props.investment.name} </strong>
                </div>
                <div className={"col-md-4 nombre-inversion"}><p><strong>Valor
                    Compra: </strong> {new Intl.NumberFormat("ES-ES", {
                    style: "currency",
                    currency: "COP"
                }).format(this.props.investment.purchaseValue)}</p>
                    <p><strong> Valor Actual: </strong> {new Intl.NumberFormat("ES-ES", {
                        style: "currency",
                        currency: "COP"
                    }).format(this.props.investment.currentValue)}</p>
                </div>
                <div className={"col-md-3 nombre-inversion"}>
                    {(this.props.investment.name.includes("CDT")) ? (
                        <React.Fragment>
                            <strong> Interes: </strong>{this.props.investment.infoComplete.referenceValue.toFixed(2) + " %"}
                        </React.Fragment>
                    ) : (this.props.investment.name.includes("Dólar") || this.props.investment.name.includes("Euro")) ?
                        (
                            <React.Fragment>
                                <strong> Monto: </strong>{this.props.investment.quantity}
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                <strong> Cantidad: </strong>{this.props.investment.quantity}
                            </React.Fragment>
                        )}
                    {(this.props.investment.indicator !== 0) ? (
                        <cite> <br/>
                            Indicador: {this.props.investment.infoComplete.type.indicator} </cite>) : (
                        <React.Fragment/>)}
                </div>
                <div className={"col-md-3 nombre-inversion"}>
                    {(this.props.investment.status) ? (<div className="row pt-2">
                        <IconButton onClick={() => {
                            this.props.addForecasts(this.props.investment, 1, this.props.id)
                            this.selectForecasts(1)
                        }}
                                    component="span" aria-label="tendencia subir" size="large"
                        ><ArrowUpwardTwoTone
                            color={(this.state.selected == 1) ? "error" : "primary"} fontSize="inherit"/></IconButton>
                        <IconButton
                            onClick={() => {
                                this.props.addForecasts(this.props.investment, -1, this.props.id)
                                this.selectForecasts(-1)
                            }}
                            component="span" aria-label="tendencia bajar" size="large"
                        ><ArrowDownwardTwoTone
                            color={(this.state.selected == -1) ? "error" : "primary"} fontSize="inherit"/></IconButton>
                        <IconButton
                            onClick={() => {
                                this.props.addForecasts(this.props.investment, 0, this.props.id)
                                this.selectForecasts(0)
                            }}
                            component="span" aria-label="tendencia igualdad" size="large"
                        ><DragHandleTwoTone
                            color={(this.state.selected == 0) ? "error" : "primary"} fontSize="inherit"/></IconButton>
                        <IconButton className={"ml-5"}>
                            {(this.props.investment.openPopper) ? ((this.props.investment.valuePopper != 0) ? (
                                <HtmlTooltip
                                    title={
                                        <React.Fragment>
                                            <Typography color="inherit">Resultado pronóstico</Typography>
                                            {"Excelente.. acabas de recibir "}<b>{'1 punto '}</b>
                                        </React.Fragment>
                                    }
                                ><CheckCircleTwoTone color={"success"}/></HtmlTooltip>
                            ) : (<HtmlTooltip className={"ml-5"}
                                              title={
                                                  <React.Fragment>
                                                      <Typography color="inherit">Resultado pronóstico</Typography>
                                                      {"En este momento no acertaste, sigue intentando"}
                                                  </React.Fragment>
                                              }
                            ><DoNotDisturbTwoTone color={"error"}/></HtmlTooltip>)) : (
                                <React.Fragment/>)}</IconButton>
                    </div>) : (<React.Fragment/>)}

                    <div className="row mt-3">
                        {(this.props.investment.history.length > 0 && !this.props.investment.name.includes("CDT") && this.props.investment.status) ? (
                            <button onClick={this.props.onClick} className="text-reset"
                                    id={this.props.id}>Vender</button>
                        ) : (
                            <React.Fragment/>
                        )}
                        {(this.props.investment.history.length > 0 && !this.props.investment.name.includes("CDT"))
                            ? (<button onClick={this.props.onClick1} className="text-reset"
                                       id={this.props.id}>Ver Grafica</button>)
                            : (<div></div>)}
                    </div>
                </div>
            </div>
            <Divider/>
        </div>;
    }
}

InvestmentDataComponent.propTypes = {
    investment: PropTypes.any,
    onClick: PropTypes.func,
    id: PropTypes.number,
    onClick1: PropTypes.func
};

export default InvestmentDataComponent;