import React, {Component} from 'react';
import axios from "axios";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Divider, IconButton,
    List,
    ListItem,
    ListItemText,
    Typography
} from "@mui/material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import RecommendationBuild from './classes/recommendation/recommendationBuild'

class Recommendation extends Component {
    objRecommendations;

    constructor(props) {
        super(props);
        this.state = {
            recommendations: [],
            expanded: "",
            infoRecommendation: [],
            viewRecommendations: [],
            news: ""
        };
//https://ourcodeworld.co/articulos/leer/327/como-ejecutar-una-funcion-del-componente-hijo-desde-el-componente-principal-en-react
        this.loadInformation();
    }

    loadInformation() {
        axios.get(`${this.props.recommendation}`).then(data => {
            let recommendations = data.data;
            this.setState({recommendations});
        }).catch(err => {
            console.log(err);
        });
    }

    calculateRecommendation() {
        let changeNews = false;
        if (this.props.news != this.state.news) {
            this.setState({
                news: this.props.news
            });
            changeNews = true;
        }
        this.objRecommendations = new RecommendationBuild(
            this.state.recommendations,
            this.props.investements,
            this.props.time,
            this.props.userGame,
            changeNews,
            this.props.totalInvestment,
            this.props.availableBalance
        );
        let infoRecommendation = this.objRecommendations.initCalculate();
        let viewRecommendations = infoRecommendation;
        this.setState({
            infoRecommendation,
            viewRecommendations
        });
    }

    handleChange(value) {
        if (this.state.expanded === value) {
            value = "";
        }
        this.setState({
            expanded: value
        });
    }

    render() {
        return (
            (this.state.viewRecommendations.length > 0) ? (
                this.state.viewRecommendations.map((recommendation, index) => (
                    <Accordion expanded={this.state.expanded === `panel${index}`} key={`panel${index}`}
                               onChange={() => this.handleChange(`panel${index}`)}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1bh-content"
                            id="panel1bh-header"
                        >
                            <Typography sx={{width: '80%', flexShrink: 0}}>
                                {recommendation.frases}
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography>
                                {recommendation.justificacion}
                                <strong> {recommendation.otras_recomendaciones}</strong>
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    /*<ListItem alignItems="flex-start">
                        <ListItemText
                            primary={recommendation.frases}
                            secondary={
                                <React.Fragment>
                                    <Typography
                                        sx={{display: 'inline'}}
                                        component="span"
                                        variant="body2"
                                        color="text.primary"
                                    >
                                        {recommendation.justificacion}
                                    </Typography>
                                    {recommendation.otras_recomendaciones}
                                </React.Fragment>
                            }
                        />
                    </ListItem>*/
                ))

            ) : (
                <div><h4>No tiene recomendaciones actualmente</h4></div>
            )
        );
    }
}

export default Recommendation;