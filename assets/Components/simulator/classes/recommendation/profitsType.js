export default class ProfitsType {
    _allRecommendations = [];
    _recommendations;
    _investments;
    _gameInfo;
    _changeNews;
    _values;
    _availableBalance;

    constructor(recommendations, investments, changeNews, values, availableBalance) {
        this._recommendations = recommendations;
        this._investments = investments;
        this._values = values;
        this._changeNews = changeNews;
        this._availableBalance = availableBalance;

        this._allRecommendations = this._recommendations.filter((recommendation) => {
            return recommendation.tipo_validacion == 'total_ganacia';
        });
    }

    getRecommendations() {
        return this._validateProfits();
    }

    _validateProfits() {
        let result = this._allRecommendations.filter((recommendation) => {
            let investment = [];
            switch (recommendation.paridad) {
                case "<":
                    let sum = 0;
                    investment = this._investments.map((investment) => {
                        sum += investment.currentValue;
                    });
                    sum += this._availableBalance;
                    return sum < this._values;
                    break;
            }
        });
        return result;
    }
}