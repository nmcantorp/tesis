export default class TimeType {
    _allRecommendations = [];
    _recommendations;
    _investments;
    _time;

    constructor(recommendations, investments, time) {
        this._recommendations = recommendations;
        this._investments = investments;
        this._time = time;
        this._allRecommendations = this._recommendations.filter((recommendation) => {
            return recommendation.tipo_validacion == 'tiempo';
        });
    }

    getRecommendations() {
        return this._validateTime();
    }

    _validateTime() {
        let result = this._allRecommendations.filter((recommendation) => {
            switch (recommendation.paridad) {
                case '<=':
                    return this._time <= recommendation.valor_validacion;
                    break;
                case 'between':
                    let values = recommendation.valor_validacion.split(";");
                    return this._time >= values[0] && this._time <= values[1];
                    break;
                case '>':
                    return this._time > recommendation.valor_validacion;
                    break;
            }
            console.table(recommendation);
        });
        return result;
    }
}