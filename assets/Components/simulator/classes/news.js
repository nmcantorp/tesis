// import {VLOOKUP} from "@formulajs/formulajs";
import {jStat} from "jstat";

export default class News {
    newsInfo = {};
    waitTime = 0;
    currentNews = "";

    constructor() {
        this.newsInfo = [
            {
                "Acumulada": 0,
                "No": 0,
                "NOTICIA": "No hay nada nuevo",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.0625,
                "No": 1,
                "NOTICIA": "Inflación en Estados Unidos llega a su nivel más alto en 39 años ",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.125,
                "No": 2,
                "NOTICIA": "Reestructuración de una de las empresas mas grandes de Colombia para reducir la deuda interna de la compañía ",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.1875,
                "No": 3,
                "NOTICIA": "Tesla anuncia que no aceptaran criptomonedas para sus transacciones.",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.25,
                "No": 4,
                "NOTICIA": "China impone nuevas restricciones al mercado de las criptomonedas con el fin de que las grandes fluctuaciones de estas no afecten el orden económico y financiero.",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.3125,
                "No": 5,
                "NOTICIA": "Se dispararon los contagios del COVID-19 frenando muchas de las actividades presenciales",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.375,
                "No": 6,
                "NOTICIA": "El Grupo Gilinski informó la puesta en marcha de una nueva oferta pública de adquisición (OPA) para adquirir el Grupo Sura.",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.4375,
                "No": 7,
                "NOTICIA": "Vacuna contra el coronavirus: Pfizer asegura que la suya es eficaz en más de un 90% mientras los expertos piden cautela",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.5,
                "No": 8,
                "NOTICIA": "Ante la posibilidad de escoger un presidente de izquierda en Colombia, Brasil se vuelva más atractiva para inversionistas.",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.5625,
                "No": 9,
                "NOTICIA": "Conflicto estalla en Ucrania luego de que Vladimir Putin le ordenara a sus fuerzas militares ingresar a dicho país. ",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.625,
                "No": 10,
                "NOTICIA": "El PIB del petrolero crece más del 3%",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.6875,
                "No": 11,
                "NOTICIA": "Banco de la República presenta mayores egresos que ingresos al cierre del mes",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.75,
                "No": 12,
                "NOTICIA": "Se presenta una gran volatilidad en los principales mercados europeos",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.8125,
                "No": 13,
                "NOTICIA": "Los precios del oro caen gracias al aumento de los rendimientos de los bonos del Tesoro de Estados Unidos ",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.875,
                "No": 14,
                "NOTICIA": "Clientes movieron $9.198 billones en el sistema financiero del país",
                "Probabilidad": 0.0625
            },
            {
                "Acumulada": 0.9375,
                "No": 15,
                "NOTICIA": "No hay nada nuevo",
                "Probabilidad": 0.0625
            }
        ]
        this.waitTime = 0;

    }

    getAllNews() {
        return this.newsInfo;
    }

    getFirstNew() {
        return this.newsInfo[0]['NOTICIA'];
    }

    updateWait() {
        let wait = this.getWaitNumber();
        // while (wait > 365) {
        //     wait = this.getWaitNumber();
        // }
        this.waitTime += wait;
    }

    setWait(value) {
        this.waitTime = value;
    }

    getWaitNumber() {
        return Math.round(((jStat.gamma.inv(Math.random(), 1, 0.1666666)) * 360) + 30);
    }

    getNews(currentTime = 0, pauseSimulator) {
        let randomNumber = Math.random();
        if ((currentTime - 1) == (this.waitTime - 1)) {
            this.currentNews = this.VLOOKUP(randomNumber, this.newsInfo, 'NOTICIA', true);
            this.updateWait();
            // if (currentTime > 0) {
            pauseSimulator();
            // }
        }
        return this.currentNews;
    }

    VLOOKUP(needle, table, index, rangeLookup) {
        var na = null;
        if (!table || !index) {
            return na;
        }
        rangeLookup = !(rangeLookup === 0 || rangeLookup === false);
        var result = na;
        var isNumberLookup = typeof needle === "number";
        var exactMatchOnly = false;
        for (var i = 0; i < table.length; i++) {
            var row = table[i];
            if (row['Acumulada'] === needle) {
                result = typeof row[index] !== 'undefined' ? row[index] : new Error("#REF!");
                break;
            } else if (!exactMatchOnly &&
                (
                    isNumberLookup &&
                    rangeLookup &&
                    row['Acumulada'] <= needle ||
                    rangeLookup &&
                    typeof row['Acumulada'] === "string" &&
                    row['Acumulada'].localeCompare(needle) < 0
                )) {
                result = typeof row[index] !== 'undefined' ? row[index] : new Error("#REF!");
            }
            if (isNumberLookup && row['Acumulada'] > needle) {
                exactMatchOnly = true;
            }
        }
        return result;
    }

}