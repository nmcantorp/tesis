import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Dialog, DialogActions, DialogContent, DialogTitle} from "@mui/material";

class VideoComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openVideoExplain: false
        }
    }

    componentDidMount() {
        this.playAndStopVideo('play')
    }

    playAndStopVideo(actionVideo) {
        let video = document.getElementById('video_explicativo');

        if (this.props.openVideoExplain) {
            if (actionVideo == 'play') {
                video.play();
            } else {
                video.pause();
                video.currentTime = 0;
            }
        }
    }

    render() {
        return (
            <div>
                <Dialog
                    fullWidth={true}
                    maxWidth={'md'}
                    open={this.props.openVideoExplain}
                    onClose={() => this.playAndStopVideo('stop')}
                    onLoad={() => document.getElementById('video_explicativo').play()}
                >
                    <DialogTitle>Video de Explicación</DialogTitle>
                    <DialogContent className={"container"} onClick={() => this.playAndStopVideo('play')}>
                        <div className="row embed-responsive">
                            <div className="col-md-auto mb-lg-5 pb-lg-5 pt-md-5 classvideo" id={"video_red"} disabled>
                                <div className="embed-responsive embed-responsive-21by9">
                                    <video id={"video_explicativo"}
                                           src={`${this.props.videoLink}`}
                                           controls>
                                        <source src={`${this.props.videoLink}`}
                                                type="video/mp4"/>
                                    </video>
                                </div>
                            </div>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.props.videoHandler(false)}>Cerrar</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

VideoComponent.propTypes = {};

export default VideoComponent;
