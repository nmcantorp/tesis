import React, {Component} from 'react';
import LoadingUtils from '../utils/LoadingUtils';
import {
    Box,
    Button, CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Fade,
    Modal
} from "@mui/material";
import axios from "axios";
// import { Player } from 'video-react';

import "video-react/dist/video-react.css";
import {Link, Redirect} from "react-router-dom";
import VideoTutorial from "../utils/VideoTutorial";

class IndexBallons extends Component {
    constructor() {
        super();
        this.state = {
            balloons: 10,
            balloonProfit: {
                "balloons_blue": 0,
                "balloons_blue1": 0,
                "balloons_blue2": 0,
                "balloons_yellow": 0,
                "balloons_yellow1": 0,
                "balloons_yellow2": 0,
                "balloons_orange": 0,
                "balloons_orange1": 0,
                "balloons_orange2": 0,
                "balloons_orange3": 0
            },
            balloonClick: {
                "balloons_blue": {"clicks": 0, "numberBalloon": 0},
                "balloons_blue1": {"clicks": 0, "numberBalloon": 0},
                "balloons_blue2": {"clicks": 0, "numberBalloon": 0},
                "balloons_yellow": {"clicks": 0, "numberBalloon": 0},
                "balloons_yellow1": {"clicks": 0, "numberBalloon": 0},
                "balloons_yellow2": {"clicks": 0, "numberBalloon": 0},
                "balloons_orange": {"clicks": 0, "numberBalloon": 0},
                "balloons_orange1": {"clicks": 0, "numberBalloon": 0},
                "balloons_orange2": {"clicks": 0, "numberBalloon": 0},
                "balloons_orange3": {"clicks": 0, "numberBalloon": 0}
            },
            numberBalloon: 0,
            profits: 0,
            vectorsClicks: [],
            balloon_selected: "",
            open: false,
            openDialog: false,
            nameBalloon: "",
            numberInflated: 0,
            widthBalloons: 4,
            clickTotal: 10,
            clicks: 0,
            status: "fail",
            balloonsProcessed: [],
            profile: "",
            loading: false,
            urlRedirect: "",
            redirect: false
        };
        this.selectBalloon = this.selectBalloon.bind(this);
        this.setOpen = this.setOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.inflateBalloon = this.inflateBalloon.bind(this);
        this.processInflateBalloon = this.processInflateBalloon.bind(this);
        this.addProfit = this.addProfit.bind(this);
        this.notContinue = this.notContinue.bind(this);
        this.explodeBalloon = this.explodeBalloon.bind(this);
        this.loading = this.loading.bind(this);
    }

    saveProgress() {
        this.loading(true);
        axios.post(this.props.images_info.configInfo.urlNewBalloon, JSON.stringify(this.state.balloonClick))
            .then(result => {
                if (result.status == 200 && result.data.error == 0) {
                    let response = result.data;
                    window.location = response.path_next;
                }
                this.loading(false);
            })
            .catch(error => {
                this.loading(false);
            });
    }

    loading(value) {
        this.setState({
            loading: value
        });
    }

    initBalloons() {
        const numberVectors = 10;
        let vectors = [];
        for (let i = 0; i < numberVectors; i++) {
            vectors[i] = Math.round(Math.random() * (7 - 4) + 4);
        }
        this.state.vectorsClicks = vectors;
    }

    setOpen(value) {
        this.setState({open: value});
    }

    componentDidMount() {
        this.refs.tutorial.handleDialog(true);
        this.loading(true);
        axios
            .get(this.props.images_info.configInfo.urlGame, {async: false})
            .then(result => {
                if (result.status == 200 && result.data.error == 0) {
                    let games = result.data.games;
                    if (typeof games.Activo != 'undefined') {
                        let game = games.Activo[0];
                        if (game.balloonTotal > 0) {
                            window.location = '/game';
                        }
                    }
                }
                this.loading(false);
            })
            .catch(error => {
                this.loading(true);
            });
        this.initBalloons();
    }

    selectBalloon(id) {
        let processed = this.state.balloonsProcessed.filter(balloon => balloon === id);
        if (processed.length > 0) {
            return false;
        }

        this.setState({balloon_selected: id});
        let allClicks = this.state.balloonClick;

        this.addInfoClicks(id, allClicks, 0);

        switch (id) {
            case 'balloons_yellow':
            case 'balloons_yellow1':
            case 'balloons_yellow2':
                this.setState({nameBalloon: 'Amarillo'});
                break;
            case 'balloons_blue':
            case 'balloons_blue1':
            case 'balloons_blue2':
                this.setState({nameBalloon: 'Azul'});
                break;
            case 'balloons_orange':
            case 'balloons_orange1':
            case 'balloons_orange2':
            case 'balloons_orange3':
                this.setState({nameBalloon: 'Naranja'});
                break;
            default:
                this.setState({nameBalloon: ''});
                break;
        }
        this.setOpen(true);
    }

    addProfit(number, reset = false) {
        let allProfit = this.state.balloonProfit;
        let allClicks = this.state.balloonClick;
        let profitOrigin = allProfit[this.state.balloon_selected];
        let clickOrigin = allClicks[this.state.balloon_selected];
        let id = this.state.balloon_selected;
        profitOrigin = (reset) ? 0 : ((profitOrigin * 2) == 0) ? 1000 : (profitOrigin * 2);
        allProfit[this.state.balloon_selected] = profitOrigin;
        this.addInfoClicks(this.state.balloon_selected, allClicks, clickOrigin.clicks + 1);

        this.setState({balloonProfit: allProfit, balloonClick: allClicks});

    }

    addInfoClicks(id_balloon, allClicks, value = 0) {
        allClicks[id_balloon].clicks = value;
        allClicks[id_balloon].numberBalloon = this.state.balloons;
    }

    inflateBalloon() {
        let number = this.state.numberInflated;
        let valueVector = this.state.vectorsClicks[this.state.balloons - 1];
        number++;
        if (number === valueVector) {
            // let balloonsPending = this.state.balloons;
            // balloonsPending = balloonsPending-1;
            this.handleClose(true);
            this.addProfit(0, true);
            this.explodeBalloon();
            // this.setState({balloons: balloonsPending, numberInflated:0, nameBalloon:"", balloon_selected:"", open: false,  widthBalloons:4})
        } else {
            this.addProfit(number);
            this.processInflateBalloon(number);
        }
    }

    explodeBalloon() {
        let idBox = this.state.balloon_selected;
        const box = document.getElementById(idBox);
        box.querySelector('img.balloons').setAttribute('style', `width: 0`);
        box.classList.add("bg-danger");
    }

    processInflateBalloon(number) {
        let idBox = this.state.balloon_selected;
        let widthOrigin = this.state.widthBalloons + 2;
        let clicks = this.state.clicks;
        clicks += 1;
        const box = document.getElementById(idBox);
        box.querySelector('img.balloons').setAttribute('style', `width: ${widthOrigin}rem`);

        this.setState({widthBalloons: widthOrigin, numberInflated: number, clicks: clicks});
    }

    notContinue() {
        let totalProfit = 0;
        let profitBalloons = Object.values(this.state.balloonProfit);

        for (const profit of profitBalloons) {
            totalProfit += profit;
        }
        this.setState({profits: totalProfit, open: false, status: 'clear', openDialog: true});
    }

    handleClose(value) {
        this.setState({openDialog: value});
        if (!value) {
            let balloonsPending = this.state.balloons;
            let processed = this.state.balloonsProcessed;
            let idBox = this.state.balloon_selected;
            const box = document.getElementById(idBox);
            balloonsPending = balloonsPending - 1;
            processed.push(this.state.balloon_selected);
            box.setAttribute('disabled', `disabled`);
            this.setState({balloonsProcessed: processed});
            this.setState({
                balloons: balloonsPending,
                numberInflated: 0,
                nameBalloon: "",
                balloon_selected: "",
                open: false,
                widthBalloons: 4
            });
            this.calculateProfile();
        }
    }

    calculateProfile() {
        let pending = this.state.balloons;
        if (pending === 1) {
            let profile = "";
            let clicks = this.state.clicks;
            let clicksTotal = this.state.clickTotal;
            if (clicks < (clicksTotal / 3)) {
                profile = "Conservador"
            } else if (clicks < (clicksTotal * 2 / 3)) {
                profile = "Moderado"
            } else {
                profile = "Arriesgado"
            }
            window.scrollTo(0, document.body.scrollHeight);
            this.setState({profile: profile});
            this.saveProgress();
        }
    }

    render() {
        const style = {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -16rem)',
            width: '50%',
            backgroundColor: '#f36060c7',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
        };
        return (
            <div>
                <LoadingUtils loadingStatus={this.state.loading}/>
                <VideoTutorial tutorial={this.props.images_info.itemVideos.globos} ref={"tutorial"}/>
                <div className="container-fluid gtco-features balloons_content" id="about">
                    <div className="container">
                        {/*<Player
                            playsInline
                            poster="/assets/poster.png"
                            src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                        />*/}
                        <div className="row">
                            <div className="col-lg-12">
                                <h4> Seleccione cada Globo para inflar: </h4>
                                <div className="row">
                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_blue"}
                                         onClick={() => this.selectBalloon("balloons_blue")} data-bs-toggle="modal"
                                         data-bs-target="#exampleModal">
                                        <h4>Ganancia Azul: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_blue)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_blue}
                                             alt=""/>
                                    </div>

                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_yellow"}
                                         onClick={() => this.selectBalloon("balloons_yellow")}>
                                        <h4>Ganancia Amarillo: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_yellow)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_yellow}
                                             alt=""/>
                                    </div>

                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_blue2"}
                                         onClick={() => this.selectBalloon("balloons_blue2")} data-bs-toggle="modal"
                                         data-bs-target="#exampleModal">
                                        <h4>Ganancia Azul: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_blue2)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_blue}
                                             alt=""/>
                                    </div>
                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_yellow2"}
                                         onClick={() => this.selectBalloon("balloons_yellow2")}>
                                        <h4>Ganancia Amarillo: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_yellow2)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_yellow}
                                             alt=""/>
                                    </div>
                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_orange"}
                                         onClick={() => this.selectBalloon("balloons_orange")}>
                                        <h4>Ganancia Naranja: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_orange)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_orange}
                                             alt=""/>
                                    </div>
                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_orange1"}
                                         onClick={() => this.selectBalloon("balloons_orange1")}>
                                        <h4>Ganancia Naranja: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_orange1)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_orange}
                                             alt=""/>
                                    </div>
                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_yellow1"}
                                         onClick={() => this.selectBalloon("balloons_yellow1")}>
                                        <h4>Ganancia Amarillo: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_yellow1)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_yellow}
                                             alt=""/>
                                    </div>
                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_orange2"}
                                         onClick={() => this.selectBalloon("balloons_orange2")}>
                                        <h4>Ganancia Naranja: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_orange2)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_orange}
                                             alt=""/>
                                    </div>
                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_blue1"}
                                         onClick={() => this.selectBalloon("balloons_blue1")} data-bs-toggle="modal"
                                         data-bs-target="#exampleModal">
                                        <h4>Ganancia Azul: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_blue1)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_blue}
                                             alt=""/>
                                    </div>
                                    <div className="col-md-4 text-center btn btn-outline-danger" id={"balloons_orange3"}
                                         onClick={() => this.selectBalloon("balloons_orange3")}>
                                        <h4>Ganancia Naranja: <br/> ${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.balloonProfit.balloons_orange3)}</h4>
                                        <img className="inflator" src={this.props.images_info.itemImages.inflator}
                                             alt=""/> <br/>
                                        <img className="balloons" src={this.props.images_info.itemImages.balloon_orange}
                                             alt=""/>
                                    </div>
                                </div>
                            </div>
                            <Link to="/game" className={"menu_principal"}>Menú <i className="fa fa-angle-left"
                                                                                  aria-hidden="true"></i></Link>
                        </div>
                    </div>
                </div>
                <Modal
                    hideBackdrop
                    open={this.state.open}
                    onClose={() => this.setOpen(false)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box style={style} className={"box-modal-balloons"}>
                        <div className="card">
                            <div className="card-header">
                                Globo {this.state.nameBalloon}
                            </div>
                            <div className="card-body">
                                <h5 className="card-title">Inflar globo</h5>
                                <div className="card-text">
                                    {this.state.numberInflated === 0 ? (
                                        <p className={"text-dark"}>
                                            Empezamos a inflar el globo {this.state.nameBalloon}?
                                        </p>
                                    ) : (
                                        <p className={"text-dark"}>
                                            Deseas seguir inflando el globo {this.state.nameBalloon}?
                                        </p>
                                    )}
                                </div>
                                <div className="row">
                                    <div className="col-md-2 col-xs-6">
                                        <button type="button" className="btn btn-success"
                                                onClick={() => this.inflateBalloon()}>Inflar!
                                        </button>
                                    </div>
                                    <div className="col-md-2 col-xs-6">
                                        <button type="button" className="btn btn-dark"
                                                onClick={() => this.notContinue()}>No Más!
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Box>
                </Modal>
                <Dialog
                    open={this.state.openDialog}
                    // onClose={this.handleClose(false)}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    {this.state.status === 'fail' ? (<div>
                        <DialogTitle id="alert-dialog-title">
                            {"Upsss, tu globo se ha reventado"}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Tu globo se ha reventado, tu ganancia fue: ${new Intl.NumberFormat("ES-ES", {
                                style: "currency",
                                currency: "COP"
                            }).format(0)}
                            </DialogContentText>
                        </DialogContent>
                    </div>) : (<div>
                            <DialogTitle id="alert-dialog-title">
                                {"Ganancias del globo"}
                            </DialogTitle>
                            <DialogContent>
                                <DialogContentText id="alert-dialog-description">
                                    Su ganancia en este globo fue: ${new Intl.NumberFormat("ES-ES", {
                                    style: "currency",
                                    currency: "COP"
                                }).format(this.state.balloonProfit[this.state.balloon_selected])}
                                </DialogContentText>
                            </DialogContent>
                        </div>
                    )}
                    <DialogActions>
                        <Button onClick={() => this.handleClose(false)}>Cerrar</Button>
                    </DialogActions>
                </Dialog>
                <div className="container-fluid gtco-numbers-block">
                    <div className="container">
                        <svg width="100%" viewBox="0 0 1600 400">
                            <defs>
                                <linearGradient id="PSgrad_03" x1="80.279%" x2="0%" y2="0%">
                                    <stop offset="0%" stopColor="rgb(1,230,248)" stopOpacity="1"/>
                                    <stop offset="100%" stopColor="rgb(29,62,222)" stopOpacity="1"/>

                                </linearGradient>

                            </defs>

                            <path fillRule="evenodd" fill="url(#PSgrad_02)" opacity="0.102"
                                  d="M801.878,3.146 L116.381,128.537 C26.052,145.060 -21.235,229.535 9.856,312.073 L159.806,710.157 C184.515,775.753 264.901,810.334 338.020,792.380 L907.021,652.668 C972.912,636.489 1019.383,573.766 1011.301,510.470 L962.013,124.412 C951.950,45.594 881.254,-11.373 801.878,3.146 Z"/>

                            <clipPath id="ctm" fill="none">
                                <path
                                    d="M98.891,386.002 L1527.942,380.805 C1581.806,380.610 1599.093,335.367 1570.005,284.353 L1480.254,126.948 C1458.704,89.153 1408.314,59.820 1366.025,57.550 L298.504,0.261 C238.784,-2.944 166.619,25.419 138.312,70.265 L16.944,262.546 C-24.214,327.750 12.103,386.317 98.891,386.002 Z"/>
                            </clipPath>

                            {/*<image clip-path="url(#ctm)" xlink:href="images/word-map.png" height="800px" width="100%"
                                   className="svg__image">

                            </image>*/}

                        </svg>
                        <div className="row">
                            <div className="col-4">
                                <div className="card">
                                    <div className="card-body">
                                        <h5 className="card-title">3</h5>
                                        <p className="card-text">Total de globos</p>
                                    </div>
                                </div>
                            </div>
                            {/*<div className="col-3">
                                <div className="card">
                                    <div className="card-body">
                                        <h5 className="card-title">{this.state.balloons}</h5>
                                        <p className="card-text">Globos por inflar</p>
                                    </div>
                                </div>
                            </div>*/}
                            <div className="col-4">
                                <div className="card">
                                    <div className="card-body">
                                        <h5 className="card-title">${new Intl.NumberFormat("ES-ES", {
                                            style: "currency",
                                            currency: "COP"
                                        }).format(this.state.profits)}</h5>
                                        <p className="card-text">Ganancia total</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="card">
                                    <div className="card-body">
                                        <h5 className="card-title">{this.state.profile}</h5>
                                        <p className="card-text">Perfil Usuario</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default IndexBallons;